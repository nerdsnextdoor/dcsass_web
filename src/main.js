import Vue from 'vue'
import App from './App.vue'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css' 
import VuePlyr from 'vue-plyr'
import 'vue-plyr/dist/vue-plyr.css'
import firebase from 'firebase'
import VueOnLineProp from "vue-online-prop"

Vue.use(Vuetify)
Vue.use(VuePlyr)
Vue.use(VueOnLineProp)

var config = {
  apiKey: "AIzaSyDu8jojYom64E_u75qKbzNysuglKn7uDmc",
  authDomain: "proyecto-2929l.firebaseapp.com",
  databaseURL: "https://proyecto-2929l.firebaseio.com",
  projectId: "proyecto-2929l",
  storageBucket: "proyecto-2929l.appspot.com",
  messagingSenderId: "862532615133"
};
firebase.initializeApp(config);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
